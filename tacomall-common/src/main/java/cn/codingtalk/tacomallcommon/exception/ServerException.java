package cn.codingtalk.tacomallcommon.exception;

public class ServerException extends RuntimeException {
    public ServerException(String message) {
        super(message);
    }
}
