package cn.codingtalk.tacomallmapper.sys;

import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.codingtalk.tacomallentity.sys.SysLog;

@Repository
public interface SysLogMapper extends BaseMapper<SysLog> {
}
