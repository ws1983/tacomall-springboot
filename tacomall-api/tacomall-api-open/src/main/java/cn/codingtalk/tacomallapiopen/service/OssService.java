package cn.codingtalk.tacomallapiopen.service;

import java.util.Map;

public interface OssService {
    Map<String, Object> authorize(String dir);
}
